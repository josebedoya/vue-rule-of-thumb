export interface Character {
  name: string;
  description: string;
  category: string;
  picture: string;
  lastUpdated: string;
  votes: Vote;
}

interface Vote {
  positive: number;
  negative: number;
}

export type Characters = Character[];

export interface CharactersProps {
  characters: Characters;
}