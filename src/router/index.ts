import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import PastTrialsView from '../views/PastTrialsView.vue'
import HowItWorksView from '../views/HowItWorksView.vue'

import MainLayout from '../components/Layout/MainLayout.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        Layout: MainLayout
      }
    },
    {
      path: '/past-trials',
      name: 'pastTrials',
      component: PastTrialsView,
      meta: {
        Layout: MainLayout
      }
    },
    {
      path: '/how-it-works',
      name: 'howItWorks',
      component: HowItWorksView,
      meta: {
        Layout: MainLayout
      }
    }
  ]
})

export default router
