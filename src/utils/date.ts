import { formatDistanceToNowStrict } from "date-fns"

export const getTimeAgo = (dateString: string): string => {
  const date = new Date(dateString);
  return formatDistanceToNowStrict(date, { addSuffix: true });
}