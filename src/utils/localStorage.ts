import type { Character } from '@/types'
import data from '@/data/data.json'

export const loadCharacters = () => {
  const savedCharacters = localStorage.getItem('characters')
  if (savedCharacters) {
    return JSON.parse(savedCharacters)
  } else {
    return data.data as Character[]
  }
}

export const saveCharacters = (characters: Character[]): void => {
  localStorage.setItem('characters', JSON.stringify(characters))
}
